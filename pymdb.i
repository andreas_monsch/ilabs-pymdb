%module pymdb

%{
  #define SWIG_FILE_WITH_INIT
  #include "mdbtools.h"
  #include "mdbsql.h"
%}

typedef char gchar;
typedef uint guint;
typedef	uint16 guint16;
typedef uint32 guint32;
typedef uint64 guint64;
typedef void* gpointer;


typedef struct {
  gpointer *pdata;
  guint	    len;
}  GPtrArray;


typedef struct {
	MdbHandle *mdb;
	int all_columns;
	int sel_count;
	unsigned int num_columns;
	GPtrArray *columns;
	unsigned int num_tables;
	GPtrArray *tables;
	MdbTableDef *cur_table;
	MdbSargNode *sarg_tree;
	GList *sarg_stack;
	/* FIX ME */
	void *bound_values[256];
	unsigned char *kludge_ttable_pg;
	long max_rows;
	char error_msg[1024];
	int limit;
	long row_count;
} MdbSQL;

typedef struct {
	MdbFile       *f;
	guint32       cur_pg;
	guint16       row_num;
	unsigned int  cur_pos;
	unsigned char pg_buf[MDB_PGSIZE];
	unsigned char alt_pg_buf[MDB_PGSIZE];
	unsigned int  num_catalog;
	GPtrArray	*catalog;
	MdbBackend	*default_backend;
	char		*backend_name;
	MdbFormatConstants *fmt;
	MdbStatistics *stats;

} MdbHandle; 

extern struct MdbSQLColumn;
extern struct MdbSQLTable;
extern struct MdbSQLSarg;

extern MdbSQL *_mdb_sql(MdbSQL *sql);
extern MdbSQL *mdb_sql_init();
extern MdbHandle *mdb_sql_open(MdbSQL *sql, char *db_name);
extern int mdb_sql_add_sarg(MdbSQL *sql, char *col_name, int op, char *constant);
extern void mdb_sql_all_columns(MdbSQL *sql);
extern void mdb_sql_sel_count(MdbSQL *sql);
extern int mdb_sql_add_column(MdbSQL *sql, char *column_name);
extern int mdb_sql_add_table(MdbSQL *sql, char *table_name);
extern char *mdb_sql_strptime(MdbSQL *sql, char *data, char *format);
extern void mdb_sql_dump(MdbSQL *sql);
extern void mdb_sql_exit(MdbSQL *sql);
extern void mdb_sql_reset(MdbSQL *sql);
extern void mdb_sql_listtables(MdbSQL *sql);
extern void mdb_sql_select(MdbSQL *sql);
extern void mdb_sql_dump_node(MdbSargNode *node, int level);
extern void mdb_sql_close(MdbSQL *sql);
extern void mdb_sql_add_or(MdbSQL *sql);
extern void mdb_sql_add_and(MdbSQL *sql);
extern void mdb_sql_add_not(MdbSQL *sql);
extern void mdb_sql_describe_table(MdbSQL *sql);
extern MdbSQL* mdb_sql_run_query (MdbSQL*, const gchar*);
extern void mdb_sql_set_maxrow(MdbSQL *sql, int maxrow);
extern int mdb_sql_eval_expr(MdbSQL *sql, char *const1, int op, char *const2);
extern void mdb_sql_bind_all(MdbSQL *sql);
extern int mdb_sql_fetch_row(MdbSQL *sql, MdbTableDef *table);
extern int mdb_sql_add_temp_col(MdbSQL *sql, MdbTableDef *ttable, int col_num, char *name, int col_type, int col_size, int is_fixed);
extern void mdb_sql_bind_column(MdbSQL *sql, int colnum, void *varaddr, int *len_ptr);
extern int mdb_sql_add_limit(MdbSQL *sql, char *limit);

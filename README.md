# Requirements

Make sure to install:

- swig
- make
- glib2.0-devel
- flex
- bison

# How to build?

Run `make`

# How to run

Start (i)python and `import pymdb`, see example.py for a minimal usage example.

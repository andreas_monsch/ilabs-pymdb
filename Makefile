
.PHONY: all wrapper libmdb clean

CFLAGS = -I include -I/usr/include/python3.9 -I/usr/include/glib-2.0 -I/lib/glib-2.0/include
LDFLAGS = -lglib-2.0

vpath %.c src/sql
vpath %.l src/sql
vpath %.y src/sql
vpath %.c src/libmdb

WRP_SRC = pymdb_wrap.c

LIB_SRC = mdbsql.c    \
          backend.c   \
          catalog.c   \
          data.c      \
          dump.c      \
          file.c      \
          iconv.c     \
          index.c     \
          like.c      \
          map.c       \
          mem.c       \
          money.c     \
          options.c   \
          props.c     \
          sargs.c     \
          stats.c     \
          table.c     \
          worktable.c \
          write.c

SQL_SRC = parser.c \
          lex.yy.c


OBJS = $(WRP_SRC:c=o) $(LIB_SRC:c=o) $(SQL_SRC:c=o) 


all: module wrapper parser sql sharedlib


module : pymdb.i
	@echo "Module"
	@swig -python $^


wrapper: pymdb_wrap.c pymdb.py
	@echo "Wrapper"
	@gcc -I/usr/include/python3.9 -I/usr/include/glib-2.0 -I/lib/glib-2.0/include -O2 -fPIC -c $<


parser: lexer.l parser.y
	@echo "Flex & Bison"
	flex $<
	bison -d $(word 2,$^)
	cp parser.tab.h parser.h
	cp parser.tab.c parser.c


sql: $(LIB_SRC) $(SQL_SRC)
	@echo "Compiling mdbsql"
	gcc -I/usr/include/glib-2.0 -I/lib/glib-2.0/include -O2 -fPIC -c $^


sharedlib: $(OBJS)
	@echo "Creating shared lib from $^"
	$(LINK.c) -shared $^ -o _pymdb.so


clean:
	rm -f *.o pymdb_wrap.c pymdb.py _pymdb.so parser.h parser.c parser.tab.h parser.tab.c lex.yy.c 
